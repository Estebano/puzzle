import Puzzle1 from '../../images/puzzle-1.jpeg';
import Puzzle2 from '../../images/puzzle-2.jpeg';
import Puzzle3 from '../../images/puzzle-3.jpeg';
import Puzzle4 from '../../images/puzzle-4.jpeg';
import Puzzle5 from '../../images/puzzle-5.jpeg';
import Puzzle6 from '../../images/puzzle-6.jpeg';
import Puzzle7 from '../../images/puzzle-7.jpeg';
import Puzzle8 from '../../images/puzzle-8.jpeg';




export const data = {
  "puzzle": [
    {
      "tileSrc" :  Puzzle1,
      "position": 1,
      "isSelected": false
    },
    {
      "tileSrc" :  Puzzle2,
      "position": 2,
      "isSelected": false
    },
    {
      "tileSrc" :  Puzzle3,
      "position": 3,
      "isSelected": false
    },
    {
      "tileSrc" :  Puzzle4,
      "position": 4,
      "isSelected": false
    },
    {
      "tileSrc" :  Puzzle5,
      "position": 5,
      "isSelected": false
    },
    {
      "tileSrc" :  Puzzle6,
      "position": 6,
      "isSelected": false
    },
    {
      "tileSrc" :  Puzzle7,
      "position": 7,
      "isSelected": false
    },
    {
      "tileSrc" : Puzzle8,
      "position": 8,
      "isSelected": false
    }
  ],
  "order" : [1, 2, 3, 4, 5, 6, 7, 8],
  "isResolved": true,
  "cols": 3,
  "rows": 3
};