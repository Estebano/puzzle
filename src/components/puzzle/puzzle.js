import React, { Component } from 'react';
import './puzzle.css';
import {data} from './puzzle-data.js';

class Puzzle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            frame: data.puzzle,
            rows: data.rows,
            cols: data.cols,
            order: data.order,
            isResolved: data.isResolved,
            toMove: null,
            toReplace: null,
        }
    }

    shuffle = () => {
       this.resetSelection();
       let shuffledArray = this.state.frame.map((item) => [Math.random(),item]).sort((item,next) => item[0] - next[0]).map((item) => item[1]);
       this.setState({
           frame: shuffledArray,
           isResolved: this.isPuzzleResolved(shuffledArray)
       });
    };

    resetSelection() {
        this.state.frame = this.state.frame.map((item) => {
            return {
                ...item,
                isSelected: false
            }
        });
    };

    isPuzzleResolved(newFrame) {
        let orderToCompare = newFrame.map(item => item.position);
        let isResolved = true;
        this.state.order.map((item, index) => {
            if(item !== orderToCompare[index]){
                isResolved = false;
            }
        });
        return isResolved;
    }

    movePuzzleItem  = (e) => {
        if(!this.state.isResolved){
            let itemIndex = e.target.getAttribute("data-attr");
            let shuffledArray = this.state.frame;
            let toMove = this.state.toMove === null ? parseInt(itemIndex) : this.state.toMove;
            let toReplace = (this.state.toReplace === null && this.state.toMove !== null) ? parseInt(itemIndex) : null;

            this.setState({
                toMove: toMove,
                toReplace: toReplace
            });

            //move tiles when both are selected
            if(toMove !== null && toReplace !== null) {

                //reverse selected tiles order
                let toMoveIndex = shuffledArray.findIndex((item) =>  item.position === toMove);
                let toReplaceIndex = shuffledArray.findIndex((item) =>  item.position === toReplace);
                let replaceItem = shuffledArray[toReplaceIndex];
                let moveItem = shuffledArray[toMoveIndex];
                shuffledArray[toMoveIndex] = replaceItem ;
                shuffledArray[toReplaceIndex] = moveItem;

                //reset selected item index
                this.setState({
                    isResolved: this.isPuzzleResolved(shuffledArray),
                    frame: shuffledArray,
                    toMove: null,
                    toReplace: null
                });
            }
        }
    };

    render() {
        let rows = Array.apply("", Array(this.state.rows));
        let cols = Array.apply("", Array(this.state.cols));
        let puzzleIndex = -1;
        return (
            <div className="puzzle">
                <div className="alert alert-info"><b>How to play :</b>
                    <ol>
                        <li>Shuffle the puzzle</li>
                        <li>Click on your target tile position</li>
                        <li>Click on the tile you want at this position</li>
                        <li>Continue until the puzzle is solved</li>
                    </ol>
                </div>
                {
                    this.state.isResolved
                    ?
                        <div className="alert alert-success"><b>Resolved</b></div>
                    :
                        <div className="alert alert-danger"><b>Please try again</b></div>
                }
                <div className="tiles">
                    {
                        rows.map( (row, i) => {
                            return (
                                <div className="row" key={i}>
                                    {
                                        cols.map( (col, y) => {
                                            puzzleIndex++;
                                            return (
                                                <div className={`tile col-${Math.floor(12/this.state.cols)}`}
                                                     onClick={(e) => this.movePuzzleItem(e)}
                                                     key={y}>
                                                    { (puzzleIndex <= (this.state.frame.length-1)) &&
                                                        <div data-attr={this.state.frame[puzzleIndex].position}>
                                                            <img src={this.state.frame[puzzleIndex].tileSrc}  className="img-fluid" alt="tile"/>
                                                        </div>
                                                    }
                                                </div>
                                            )
                                        })

                                    }
                                </div>
                            )
                        })
                    }
                </div>
                <div className="button-wrapper">
                    <button onClick={this.shuffle} className="btn btn-primary">Shuffle</button>
                </div>

            </div>
        );
    }
}

export default Puzzle;
