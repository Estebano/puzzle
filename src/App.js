import React, { Component } from 'react';
import Puzzle from './components/puzzle/puzzle';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="container">
        <Puzzle />
      </div>
    );
  }
}

export default App;
